### BK3432简介



BK3432是一个高度集成的5.0双模蓝牙数据SoC芯片，支持2mbps数据速率。它集成了高性能的射频收发器、基带、单片机、功能丰富的外设单元、可编程的协议和配置文件，支持蓝牙经典和低能耗应用。内置Flash架构使它适合于定制的应用程序。



BK3431Q|BK3432 可替代 cc2541，nrf51822，TLSR8266，DA14580等市面主流ble芯片。



### 特性



兼容蓝牙SIG和蓝牙双模4.2

低功耗2.4GHz收发器
ARM968E核微处理器集成电路


#### 特性

1.蓝牙®SIG蓝牙双模5.0	
2.低功耗2.4 ghz收发器
3.单片机集成	
4.160 KB可编程闪存用于程序，20KB RAM用于数据
5.程序代码读保护	
6.工作电压从0.9 V到3.6 V
7.时钟
16MHz晶体基准时钟
64MHz数字锁相环时钟
32KHz环形振荡器
外部32KHz晶体振荡器
单片机可以运行在任何带有内部分频器的时钟源上	
8.接口和外围设备
JTAG, I2C, SPI接口
两组UART接口
多路PWM输出
片上10位通用ADC
具有多路复用接口功能的GPIO
真随机数发生器
9.典型的封装类型
32-pin QFN 4x4	


### 实物图
BK3432Q双模BLE 经典5.0蓝牙SoC QFN32封装(图1)
![输入图片说明](http://bbs.sunsili.com/data/attachment/forum/202203/31/234048kojvukmr0tdnf2uv.jpg)

封装
![输入图片说明](http://bbs.sunsili.com/data/attachment/forum/202203/31/234048i5wotebwg5dgdztb.jpg)

BK3432Q双模BLE 经典5.0蓝牙SoC QFN32封装(图2) BK3432Q双模BLE 经典5.0蓝牙SoC QFN32封装(图3) BK3432Q双模BLE 经典5.0蓝牙SoC QFN32封装(图4)

资料下载


[BK3432SDK软件硬件开发文档下载_资料下载_光明谷科技 (sunsili.com)](http://www.sunsili.com/html/support/downloads/221.html)

#### 软件架构
软件架构说明
├─binaries
 ├─libs
 ├─projects
 │  ├─bim
 │  │  ├─app
 │  │  ├─lnk
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      └─bim
 │  ├─bim_1
 │  │  ├─app
 │  │  ├─lnk
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      └─bim
 │  ├─ble_app_ancs
 │  │  ├─app
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  ├─ble_app_gatt
 │  │  ├─app
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  ├─ble_app_gatt_1
 │  │  ├─app
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  ├─ble_app_gatt_128
 │  │  ├─app
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  ├─ble_app_gatt_led
 │  │  ├─app
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  ├─ble_app_gatt_long_pdu_advertising
 │  │  ├─app
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  ├─ble_app_gatt_muti_chars
 │  │  ├─app
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  ├─ble_app_gatt_scan_advertising
 │  │  ├─app
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  ├─ble_app_gma_1
 │  │  ├─app
 │  │  │  └─gma
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  ├─ble_app_remote
 │  │  ├─app
 │  │  ├─config
 │  │  ├─lst
 │  │  ├─obj
 │  │  └─output
 │  │      ├─app
 │  │      ├─bim
 │  │      └─stack
 │  └─ble_app_wechat
 │      ├─app
 │      ├─config
 │      ├─lst
 │      ├─obj
 │      ├─output
 │      │  ├─app
 │      │  ├─bim
 │      │  └─stack
 │      └─wechat
 └─sdk
     ├─ble_stack
     │  ├─com
     │  │  ├─prf
     │  │  ├─profiles
     │  │  │  ├─ancs
     │  │  │  │  └─ancsc
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─anp
     │  │  │  │  ├─anpc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─anps
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─bas
     │  │  │  │  ├─basc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─bass
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─blp
     │  │  │  │  ├─blpc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─blps
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─cpp
     │  │  │  │  ├─cppc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─cpps
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─cscp
     │  │  │  │  ├─cscpc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─cscps
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─dis
     │  │  │  │  ├─disc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─diss
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─FCC0
     │  │  │  │  ├─api
     │  │  │  │  ├─src
     │  │  │  │  └─util
     │  │  │  ├─FEE0
     │  │  │  │  ├─api
     │  │  │  │  └─src
     │  │  │  ├─FFE0
     │  │  │  │  ├─api
     │  │  │  │  └─src
     │  │  │  ├─FFF0
     │  │  │  │  ├─api
     │  │  │  │  └─src
     │  │  │  ├─find
     │  │  │  │  ├─findl
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─findt
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─glp
     │  │  │  │  ├─glpc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─glps
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─hogp
     │  │  │  │  ├─hogpbh
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  ├─hogpd
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  ├─hogpm
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─hogprh
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─hrp
     │  │  │  │  ├─hrpc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─hrps
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─htp
     │  │  │  │  ├─htpc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─htpt
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─lan
     │  │  │  │  ├─lanc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─lans
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─oad
     │  │  │  │  ├─api
     │  │  │  │  └─src
     │  │  │  ├─pasp
     │  │  │  │  ├─paspc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─pasps
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─prox
     │  │  │  │  ├─proxm
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─proxr
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─rscp
     │  │  │  │  ├─rscpc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─rscps
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─scpp
     │  │  │  │  ├─scppc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─scpps
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─tip
     │  │  │  │  ├─tipc
     │  │  │  │  │  ├─api
     │  │  │  │  │  └─src
     │  │  │  │  └─tips
     │  │  │  │      ├─api
     │  │  │  │      └─src
     │  │  │  ├─tuya
     │  │  │  │  ├─api
     │  │  │  │  └─src
     │  │  │  └─wechat
     │  │  │      ├─api
     │  │  │      └─src
     │  │  ├─rwble
     │  │  ├─rwble_hl
     │  │  └─rwip
     │  │      ├─api
     │  │      └─src
     │  └─inc
     │      ├─ahi
     │      ├─ble
     │      │  ├─hl
     │      │  │  ├─gap
     │      │  │  │  ├─gapc
     │      │  │  │  ├─gapm
     │      │  │  │  ├─smpc
     │      │  │  │  └─smpm
     │      │  │  ├─gatt
     │      │  │  │  ├─attc
     │      │  │  │  ├─attm
     │      │  │  │  ├─atts
     │      │  │  │  ├─gattc
     │      │  │  │  └─gattm
     │      │  │  └─l2c
     │      │  │      ├─l2cc
     │      │  │      └─l2cm
     │      │  └─ll
     │      │      ├─em
     │      │      ├─llc
     │      │      ├─lld
     │      │      └─llm
     │      ├─ea
     │      ├─em
     │      ├─h4tl
     │      ├─hci
     │      ├─ke
     │      └─nvds
     ├─plactform
     │  ├─arch
     │  │  ├─boot
     │  │  ├─compiler
     │  │  ├─ll
     │  │  └─main
     │  ├─core_modules
     │  │  ├─arch_console
     │  │  ├─common
     │  │  │  ├─api
     │  │  │  └─src
     │  │  ├─dbg
     │  │  │  ├─api
     │  │  │  └─src
     │  │  ├─ecc_p256
     │  │  │  ├─api
     │  │  │  └─src
     │  │  └─rf
     │  │      ├─api
     │  │      └─src
     │  ├─driver
     │  │  ├─adc
     │  │  ├─coex
     │  │  ├─emi
     │  │  ├─flash
     │  │  ├─gpio
     │  │  ├─i2c
     │  │  ├─icu
     │  │  ├─intc
     │  │  ├─ir
     │  │  ├─led
     │  │  ├─pwm
     │  │  ├─rc_cal
     │  │  ├─reg
     │  │  ├─rtc
     │  │  ├─spi
     │  │  ├─syscntl
     │  │  ├─timer
     │  │  ├─uart
     │  │  ├─uart2
     │  │  ├─utc
     │  │  └─wdt
     │  ├─reg
     │  └─rom
     │      ├─common
     │      └─hci
     └─project_files  

 project存放Keil例程, 用MDK510或MDK470版本, 高版本编译不了, 没有Arm9支持包.
│  ├─bim_1                          不带协议栈做普通MCU例程
│  ├─ble_app_ancs                与IOS通信例程
│  ├─ble_app_gatt                GATT数据通信例程
│  ├─ble_app_gatt_1            改进版GATT数据通信例程
│  ├─ble_app_gatt_128        改进版GATT(支持128Bytes)数据通信例程
│  ├─ble_app_gatt_long_pdu_advertising 广播例程
│  ├─ble_app_gatt_muti_chars   改进版GATT(支持多字符串)数据通信例程
│  ├─ble_app_gatt_scan_advertising  广播扫描例程
│  ├─ble_app_gma_1         连接天猫精灵---数据通信例程
│  ├─ble_app_remote         蓝牙键盘鼠标例程
│  └─ble_app_wechat         连接微信---数据通信例程


#### 安装教程

1.  下载本地（不要有中文目录）请到[BK3432SDK软件硬件开发文档下载 http://bbs.sunsili.com/thread-225501-1-1.html](http:// http://bbs.sunsili.com/thread-225501-1-1.html)
2.  解压（不要有中文目录， 要不然编译会出现莫名其妙的问题）
3.  project存放Keil例程，双击打开，要有Keil5

#### 使用说明

1.   project存放Keil例程，复制你需要的例程
2.   双击工程
3.   修改代码，编译、下载目标板

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
